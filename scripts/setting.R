#######################
## Setting Nestle    ##
#######################


# Carregando os pacotes ---------------------------------------------------

source("C:/Egnyte/Shared/operacional/modelos/Seletor/load_functions.R")


# VARIÁVEIS DEPENDENTES --------------------------------------------------------------------------

dados_y <-  read_excel(path = "./inputs/dados/DATA FOR SOY OIL CATEGORY_COFCO.xlsx", 
                       sheet = "Y", 
                       range = "A1:C75",
                       col_names = TRUE) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)


oleo_soja <- dados_y %>% select(1, 2)
p_nielsen <- dados_y %>% select(1, 3)


# VARIAVEIS EXPLICÁTIVAS (X) -----------------------------------------------------------------

# CORE----------------------------------------------------------------------------------------

series_macro <- "C:/Egnyte/Shared/operacional/modelos/dados_macro_modelagem/explicativas.xlsx"

pib_br <-  read_excel(path = series_macro, 
                      sheet = "pib", 
                      range = "C11:AJ683") %>% 
  select(data_tidy = 1, pib_br = BR) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)

pib_br_lag1 <- pib_br %>% rename(pib_br_lag1 = pib_br)
pib_br_lag2 <- pib_br %>% rename(pib_br_lag2 = pib_br)


pmcr_br <- read_excel(path = series_macro, 
                      sheet = "pmc_r", 
                      range = "C11:AJ683") %>% 
  select(data_tidy = 1, pmcr_br = BR) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)

pmcr_br_lag1 <- pmcr_br %>% rename(pmcr_br_lag1 = pmcr_br)
pmcr_br_lag2 <- pmcr_br %>% rename(pmcr_br_lag2 = pmcr_br)


pmca_br <- read_excel(path = series_macro, 
                      sheet = "pmc_a", 
                      range = "C11:AJ683") %>% 
  select(data_tidy = 1, pmca_br = BR) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)

pmca_br_lag1 <- pmca_br %>% rename(pmca_br_lag1 = pmca_br)
pmca_br_lag2 <- pmca_br %>% rename(pmca_br_lag2 = pmca_br)


massa_br <- read_excel(path = series_macro, 
                       sheet = "massa_r", 
                       range = "A1:B587") %>% 
  select(data_tidy = 1, massa_br = massar) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)

massa_br_lag1 <- massa_br %>% rename(massa_br_lag1 = massa_br)
massa_br_lag2 <- massa_br %>% rename(massa_br_lag2 = massa_br)


renda_br <- read_excel(path = series_macro, 
                       sheet = "renda_r", 
                       range = "A1:B587") %>% 
  select(data_tidy = 1, renda_br = rendar) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)

renda_br_lag1 <- renda_br %>% rename(renda_br_lag1 = renda_br)
renda_br_lag2 <- renda_br %>% rename(renda_br_lag2 = renda_br)



# PREÇOS  -----------------------------------------------------------------------------------

dados_preco <-  read_excel("./inputs/dados/ipca_bens_subs.xlsx",
                           sheet = "preco_relativo",
                           range = "I51:M125",
                           col_names = TRUE) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)


p_ipca <- dados_preco %>% select(data_tidy, p_ipca)
prel_mant <- dados_preco %>% select(data_tidy, prel_mant)
prel_azeite <- dados_preco %>% select(data_tidy, prel_azeite)
prel_marg <- dados_preco %>% select(data_tidy, prel_marg)


# DEMAIS EXPLICATIVAS -----------------------------------------------------------------------

icc <- read_excel(path = series_macro, 
                  sheet = "icc", 
                  range = "A2:B546") %>% 
  rename(data_tidy = 1) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)



du <- read_excel(path = series_macro, 
                 sheet = "dias_uteis", 
                 range = "A2:B624") %>% 
  rename(data_tidy = 1,
         z_du = dias_uteis) %>% 
  mutate_at(vars(data_tidy), lubridate::as_date)






# GERANDO OS DATASETS -----------------------------------------------------------------------

for (layer_1 in c("oleo_soja")) {
  
  for(layer_2 in c("massa_br", "pmcr_br", "pmca_br", "pib_br", "renda_br",
                   "massa_br_lag1", "pmcr_br_lag1", "pmca_br_lag1", "pib_br_lag1", "renda_br_lag1",
                   "massa_br_lag2", "pmcr_br_lag2", "pmca_br_lag2", "pib_br_lag2", "renda_br_lag2")) {
    
    for(layer_3 in c("p_ipca", "p_nielsen")) {
      
      
      # Variável Dependente
      Y <- get(layer_1)
      
      # Adiciona variáveis explicativas em camadas
      dataset <- Y %>% 
        left_join(get(layer_2)) %>% 
        left_join(get(layer_3))
      
      
      # Adiciona as demais variáveis explicativas
      dataset <- dataset %>% 
        left_join(prel_marg) %>% 
        left_join(prel_mant) %>% 
        left_join(prel_azeite) %>% 
        left_join(icc) %>% 
        left_join(du)        
      
      
      # Precaução: força o formato numérico
      dataset <- dataset %>% mutate_at(vars(-"data_tidy"), as.numeric)
      
      # Salva os datasets
      saveRDS(dataset,
              paste("./inputs/datasets/v1/dataset", layer_1, layer_2, layer_3, sep = "_"))
      
      
    }
  }
}






